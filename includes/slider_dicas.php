
<style media="screen">

.carroucel_dicas .carousel-indicators li {
    width: 22px;
    height: 22px;
    border-radius: 50%;
    background:#757575;
}

.carroucel_dicas .carousel-indicators .active {
  background-color: #000;
}
.carroucel_dicas .carousel-indicators {
    left: inherit;
    right: 9%;
    margin-right: inherit;
    bottom: -26px;
}

.carroucel_dicas .controle .fa{
  color: #001d33;
}

.carousel-control-next, .carousel-control-prev {
    width: 5%;
    height: 7%;
    top: inherit;
    bottom: -8%;
  }

.carroucel_dicas .carousel-control-next {
  right: 0px;
}

.carroucel_dicas .carousel-control-prev {
  left: 70%;
}

.carroucel_dicas .card{
background: none;
border: 0;
}
.carroucel_dicas .card-body{
  padding: 5px 0;
}

</style>
<?php
$qtd_itens_linha = 2; // quantidade de itens por linha
$id_slider = "carouselExampleIndicators";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="carroucel_dicas top50 bottom100 carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_dicas","limit 6");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_dicas", "limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>

        <div class="carousel-item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>

                <div class="col-6 pull-left">
                  <div class="card">
                    <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($linha[$c_temp][url_amigavel]); ?>" title="<?php Util::imprime($linha[$c_temp][titulo]); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 370, 268, array("class"=>"card-img-top w-100 rounded", "alt"=>"$linha[$c_temp][titulo]")) ?>
                    </a>
                    <div class="card-body">
                      <div class=" desc_dica">
                        <p class="text-uppercase text-left"><?php Util::imprime($linha[$c_temp][titulo],200); ?></p>
                      </div>

                    </div>
                  </div>
                </div>


              <?php /*

              <!-- <div class="col-xs-8 dicas_descricao_home top20 padding0">
              <p><?php Util::imprime($linha[$c_temp][descricao], 300) ?></p>
              </div>
              */ ?>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>



  <a class="carousel-control-prev controle" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>
    <span class="sr-only">prev</span>
  </a>
  <a class="carousel-control-next controle" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>
</div>
