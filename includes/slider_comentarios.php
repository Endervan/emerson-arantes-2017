
<style media="screen">
.carroucel_comentarios .carousel-indicators li {
  width: 0px;
  height: 0px;
  border-radius: 50%;
  background:#757575;
}
.carroucel_comentarios .carousel-indicators .active {background-color: #000;}
.carroucel_comentarios .carousel-indicators {
  margin-right: 40%;
  margin-left: 40%;
  bottom: -26px;
}
.carroucel_comentarios .controle .fa{color: #001d33;}
.carousel-control-next, .carousel-control-prev {
  width: 5%;
  height: 7%;
  top: inherit;
  bottom: -8%;
}

.carroucel_comentarios .carousel-control-next {right: 45%;}
.carroucel_comentarios .carousel-control-prev {left: 45%;}
.carroucel_comentarios .card{background: none;border: 0;}
.carroucel_comentarios .card-body{padding: 5px 0;}
.carroucel_comentarios p{font-size: 1.13rem /* 18/16 */;}
</style>
<?php
$qtd_itens_linha = 1; // quantidade de itens por linha
$id_slider = "carouselExampleIndicators1";
unset($linha);



?>

<!-- <script type="text/javascript">
$('.carroucel_comentarios').on('slide.bs.carousel', function () {
  interval: 2000
})

</script> -->




<div id="<?php echo $id_slider ?>" class="carroucel_comentarios top50 bottom100 carousel slide" data-ride="carousel"  data-interval="20000">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_comentarios","limit 6");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_comentarios", "limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>

        <div class="carousel-item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>

              <div class="col-12 text-center pull-left">
                <div class="card">
                  <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 174, 174, array("class"=>"rounded-circle", "alt"=>"$linha[$c_temp][titulo]")) ?>
                  <div class="card-body">
                    <?php /*
                    <div class="desc_comentario top15">
                    <h2 class="text-uppercase "><span><?php Util::imprime($linha[$c_temp][titulo]); ?></span></h2>
                    </div>
                    */ ?>

                    <div class=" desc_comentario_home p15 ">
                      <p class="text-uppercase "><?php Util::imprime($linha[$c_temp][descricao]); ?></p>
                    </div>

                  </div>
                </div>
              </div>


              <?php /*

              <!-- <div class="col-xs-8 dicas_descricao_home top20 padding0">
              <p><?php Util::imprime($linha[$c_temp][descricao], 300) ?></p>
              </div>
              */ ?>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>


  <a class="carousel-control-prev controle" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <i class="fa fa-arrow-circle-o-left fa-2x" aria-hidden="true"></i>
    <span class="sr-only">prev</span>
  </a>
  <a class="carousel-control-next controle" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <i class="fa fa-arrow-circle-o-right fa-2x" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>
</div>
