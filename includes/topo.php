
<div class="container-fluid bg_topo">
  <div class="row">

    <div class="container ">
      <div class="row">
        <?php
        if(empty($voltar_para)){
          $link_topo = Util::caminho_projeto()."/";
        }else{
          $link_topo = Util::caminho_projeto()."/".$voltar_para;
        }
        ?>

        <div class="col-3 padding0 top20 bottom20 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>

        <div class="col-9">
          <div class=" top25">
            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->
            <div class="telefone_topo">
              <div class="row">
                <div class="col-3 ml-auto padding0 media">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
                  <div class="media-body align-self-bottom">
                    <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h4>
                  </div>
                </div>
                <div class="col-3 padding0 media">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
                  <div class="media-body align-self-center">
                    <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h4>
                  </div>
                </div>

                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->

                <div class="col-4 avaliacao_topo  carrinho">

                  <a href="<?php echo Util::caminho_projeto(); ?>/contatos" class="btn btn_topo">
                    <img src="<?php echo Util::caminho_projeto(); ?>/imgs/marca_consilta.png" alt="" />
                  </a>
                    <?php /*
                  <!-- Large modal -->
                  <a type="a" class="btn btn_topo" data-toggle="modal" data-target=".bd-example-modal-lg">
                    <img src="<?php echo Util::caminho_projeto(); ?>/imgs/marca_consilta.png" alt="" />

                  </a>



                  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h5 class="modal-title" id="myLargeModalLabel">SELECIONE A CONSULTA DESEJADA</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="row modal-body modal-geral">

                          <?php
                          $i=0;
                          $result = $obj_site->select("tb_tratamentos");
                          if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result)){

                              ?>

                              <div class="col-12 top10">
                                <div class="media">
                                  <h2 class="text-uppercase align-self-center mr-3"><?php Util::imprime($row[titulo]); ?></h2>

                                  <div class="media-body">
                                    <a href="javascript:void(0);" class="btn btn-danger pull-right" title="MARCA CONSULTA" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'servico'">
                                      AGENDAR
                                    </a>
                                  </div>
                                </div>


                              </div>


                              <?php
                              if ($i==0) {
                                echo "<div class='clearfix'>  </div>";
                              }else {
                                $i++;
                              }
                            }
                          }


                          ?>


                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fecha</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  */ ?>

                </div>
                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->

              </div>
            </div>

            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->



            <div class="clearfix">  </div>
            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->
            <div class="col-12 top20  text-center menu">
              <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
              <div id="navbar">
                <ul class="nav navbar-nav navbar-right align-self-center">
                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/">HOME
                    </a>
                  </li>

                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/empresa">DR EMERSON ARANTES
                    </a>
                  </li>

                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "tratamentos" or Url::getURL( 0 ) == "tratamento"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/tratamentos">TRATAMENTOS
                    </a>
                  </li>

                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
                    </a>
                  </li>

                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == "convenios" or Url::getURL( 0 ) == "convenio"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/convenios">CONVÊNIOS
                    </a>
                  </li>

                  <?php /*
                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == "trabalhos-sociais"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/trabalhos-sociais">TRABALHO SOCIAL
                    </a>
                  </li>

                  */ ?>

                  <li class="top10">
                    <a class="<?php if(Url::getURL( 0 ) == "contatos" or Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS
                    </a>
                  </li>
                </ul>
              </div><!--/.nav-collapse -->

            </div>
            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->

          </div>



        </div>

      </div>
    </div>


  </div>
</div>
