
<div class="container-fluid rodape">
	<div class="row">
		<div class="container bottom20">
			<div class="row ">
				<div class="col-10 menu_rodape ">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav nav-right">
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">DR EMERSON ARANTES
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "tratamentos" or Url::getURL( 0 ) == "tratamento" or Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/tratamentos">TRATAMENTOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "convenios"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/convenios">CONVÊNIOS
							</a>
						</li>

<?php /*
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "trabalhos-sociais"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/trabalhos-sociais">TRABALHO SOCIAL
							</a>
						</li>

*/ ?>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS
							</a>
						</li>


					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>

				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-2 text-right top10 redes">
					<div class="">
						<?php if ($config[instagram] != "") { ?>
							<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
								<img class="mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/instragram.png" alt="">
							</a>
						<?php } ?>
						<?php if ($config[facebook] != "") { ?>
							<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
								<img class="mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/face.png" alt="">
							</a>
						<?php } ?>
						<?php if ($config[google_plus] != "") { ?>
							<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/google.png" alt="">
							</a>
						<?php } ?>
					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->



				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-3 text-center top10">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<div class="col-8 padding0 top30">
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="telefone_topo">
						<div class="row">
							<div class="col-4 media">
								<img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
								<div class="media-body align-self-bottom">
									<h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h4>
								</div>
							</div>
							<div class="col-3 padding0 media mr-auto">
								<img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
								<div class="media-body align-self-center">
									<h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h4>
								</div>
							</div>

							<div class="col-12 media top30">
								<img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_local.png" alt="">
								<div class="media-body align-self-center">
									<p class="mt-0 ml-2"><span><?php Util::imprime($config[endereco]); ?></span></p>
								</div>
							</div>


						</div>
					</div>

					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
				</div>


				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->
				<div class="col redes top30">
					<div class="row">

						<div class="col-3">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>

					</div>
				</div>
				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright EMERSON ARANTES</h5>
		</div>
	</div>
</div>
