<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",8) ?>
<style>
.bg-interna{
  background: #ececec url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">

      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8);?>
      <div class="col-12 text-center top150 bottom100">
        <h3> <?php Util::imprime($row[legenda_1]) ?></h3>
        <h3><span><?php Util::imprime($row[legenda_2]) ?></span></h3>
      </div>


      <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
      <div class="col-12 text-center top50">
        <div class="">
          <p><?php Util::imprime($row1[descricao]) ?></p>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



    <!--  ==============================================================  -->
    <!--   NOSSOS TRABALHOS -->
    <!--  ==============================================================  -->
    <div class="container bottom100">
      <div class="row servicos">

        <?php
        $result = $obj_site->select("tb_trabalhos");
        if(mysql_num_rows($result) > 0){
          $i = 0;
          while($row = mysql_fetch_array($result)){
            if ($i == 0) {
              $order = 'order-1';
              $order1 = 'order-12';
              $div = 'container_metade_esquerda';
              $i++;
            }else{
              $order = 'order-12';
              $order1 = 'order-1';
              $div = 'container_metade_direita';
              $i = 0;
            }
            ?>

            <div class="relativo top120">
              <div class="<?php echo $div; ?>"></div>
              <div class="row">
                <div class="col-6 top10 <?php echo $order;?>">
                  <h2 class="text-uppercase"><span><?php Util::imprime($row[titulo]); ?></span></h2>
                  <div class="top10"><p><?php Util::imprime($row[descricao],150); ?></p></div>
                </div>
                <div class="col-6 <?php echo $order1;?> top15">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 570, 354, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>

                </div>
              </div>
            </div>



            <?php
            if ($i==0) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>


      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   NOSSOS TRABALHOS -->
    <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
