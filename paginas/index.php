<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">
      <div id="container_banner">
        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--   TRATAMENTOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
    <div class="row tratamento_home1">
      <div class="container bottom40">
        <div class="row ">
          <div class="col-12 text-center top40">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
            <h5 class="mt-0">NOSSOS <?php Util::imprime($row[titulo]) ?></h5>
            <img  src="<?php echo Util::caminho_projeto() ?>/imgs/barra.png" alt="" />
            <div class="col-10 home col-offset-1 top20">
              <p class="text-left"><?php Util::imprime($row[descricao]) ?></p>
            </div>
          </div>

          <?php
          $result = $obj_site->select("tb_tratamentos","limit 4");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              $i=0;
              ?>

              <div class="col-6 tratamento_home text-center top40">
                <div class="card">
                  <a href="<?php echo Util::caminho_projeto() ?>/tratamento/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <img width="120" height="120" class=" top35 media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[imagem]); ?>" />


                  <div class="card-body">
                    <div>
                      <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
                    </div>
                    <div class="top15">
                      <p class="card-text desc_tratamento"><?php Util::imprime($row[descricao],200); ?></p>
                    </div>
                    </a>
                  </div>
                </div>
              </div>

              <?php
              if ($i==1) {
                echo "<div class='clearfix'>  </div>";
              }else {
                $i++;
              }
            }
          }
          ?>

        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   TRATAMENTOS -->
  <!--  ==============================================================  -->




    <!--  ==============================================================  -->
    <!--   O QUE ELES DIZEM -->
    <!--  ==============================================================  -->
    <div class="container-fluid">
      <div class="row comemtario_home">
        <div class="container bottom40">
          <div class="row ">
            <div class="col-12 top60">
              <h5 class="mt-0">O QUE ELAS DIZEM</h5>
              <img  src="<?php echo Util::caminho_projeto() ?>/imgs/barra.png" alt="" />
            </div>

          <div class="col-8 padding0">
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/slider_comentarios.php') ?>
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->

          </div>


          </div>
        </div>


      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   O QUE ELES DIZEM -->
    <!--  ==============================================================  -->




  <!--  ==============================================================  -->
  <!--  ======================================= SOBRE A EMPRESA ========-->
  <div class="container  bottom40">
    <div class="row ">
      <div class="col-12 text-center top60">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <h5 class="mt-0"><?php Util::imprime($row[titulo]) ?></h5>
        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/barra.png" alt="" />
        <div class="top20">
          <p class="text-left"><?php Util::imprime($row[descricao]) ?></p>
        </div>

        <div class="text-right">
          <a class="btn btn_saiba_home top35" href="<?php echo Util::caminho_projeto() ?>/empresa" title="SAIBA MAIS">
            SAIBA MAIS
          </a>
        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   SOBRE A EMPRESA -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--   LOCALIDADE E TELEFONES -->
  <!--  ==============================================================  -->
  <div class="container ">
    <div class="row ">
      <div class="col-4 localidade">
        <div class="media">
          <img class="align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_atentimento.png" alt="">
          <div class="media-body">
            <h2 class="mt-0">ATENDIMENTO</span></h2>
          </div>
        </div>
        <img class="bottom20 top10" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_atentimento.png" alt="">
        <div class="col-12 media">
          <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
          <div class="media-body align-self-center">
            <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h4>
          </div>
        </div>
        <div class="col-12 media">
          <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
          <div class="media-body align-self-bottom">
            <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h4>
          </div>
        </div>



        <div class="media top25">
          <img class="align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="">
          <div class="media-body">
            <h2 class="mt-0">COMO CHEGAR</span></h2>
          </div>
        </div>
        <img class="bottom20 top10" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_atentimento.png" alt="">
        <div class="col-12">
          <p class="text-left"><?php Util::imprime($config[endereco]); ?></p>
        </div>

      </div>

      <div class="col-8 bottom50 padding0">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   LOCALIDADE E TELEFONES -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
    <div class="row bg_dicas">

      <div class="container">
        <div class="row ">
          <div class="col-7 ml-auto top60">
            <h5 class="mt-0">NOSSAS DICAS</h5>
            <img  src="<?php echo Util::caminho_projeto() ?>/imgs/barra.png" alt="" />
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->
            <?php require_once('./includes/slider_dicas.php') ?>
            <!-- ======================================================================= -->
            <!-- rodape    -->
            <!-- ======================================================================= -->
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true,
        delay:  10000
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: false,/*tira bolinhas*/
    animationLoop: true,
    itemWidth: 190,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
