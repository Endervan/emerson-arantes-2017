<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #ececec url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">

      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2);?>
      <div class="col-12 text-center top150 bottom120">
        <h3> <?php Util::imprime($row[legenda_1]) ?></h3>
        <h3><span><?php Util::imprime($row[legenda_2]) ?></span></h3>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--   DICAS ==========================================================-->

  <div class="container">
    <div class="row dicas bottom100">

      <?php
      $result = $obj_site->select("tb_dicas");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          $i=0;
          ?>

          <div class="col-4 top20">
            <div class="card">
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 268, array("class"=>"card-img-top w-100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="card-body">
                <div class=" desc_dica">
                  <p class="text-uppercase "><?php Util::imprime($row[titulo]); ?></p>
                </div>

              </div>
            </div>
          </div>

          <?php
          if ($i==2) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>

    </div>
  </div>


  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
