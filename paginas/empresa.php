<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 108px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container ">
    <div class="row titulo_empresa">

      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1);?>
      <div class="col-12 text-center top150 bottom80">
        <h3> <span><?php Util::imprime($row[legenda_1]) ?></span></h3>
        <h6><?php Util::imprime($row[legenda_2]) ?></h6>
      </div>


      <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
      <div class="col-8 top50 text-center">
        <p><span><?php Util::imprime($row1[descricao]) ?></span></p>

        <?php $row2 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="text-center top50 bottom80">
          <p><span><?php Util::imprime($row2[descricao]) ?></span></p>
        </div>

      </div>

      <div class="col-4 bg-dremerson top60">

      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <?php /*
  <!--  ==============================================================  -->
  <!--   DESCRICOES -->
  <!--  ==============================================================  -->
  <div class="container">
  <div class="row emp_descricao">
  <?php $row2 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
  <?php $row3 = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
  <?php $row4 = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
  <?php $row5 = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>

  <div class="col-4 text-center top80 card">
  <img width="50" height="70" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa01.png" alt="">
  <div class="card-body">
  <h1 class="top10"><?php Util::imprime($row3[titulo]) ?></h1>
  <p class="card-text top10"><?php Util::imprime($row3[descricao]) ?></p>
  </div>
  </div>

  <div class="col-4 text-center top80 card">
  <img width="50" height="70" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa02.png" alt="">
  <div class="card-body">
  <h1 class="top10"><?php Util::imprime($row4[titulo]) ?></h1>
  <p class="card-text top10"><?php Util::imprime($row4[descricao]) ?></p>
  </div>
  </div>

  <div class="col-4 text-center top80 card">
  <img width="50" height="70" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa03.png" alt="">
  <div class="card-body">
  <h1 class="top10"><?php Util::imprime($row5[titulo]) ?></h1>
  <p class="card-text top10"><?php Util::imprime($row5[descricao]) ?></p>
  </div>
  </div>


  </div>
  </div>
  <!--  ==============================================================  -->
  <!--   DESCRICOES -->
  <!--  ==============================================================  -->
  */ ?>

  <!--  ==============================================================  -->
  <!--   LOCALIDADE E TELEFONES -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row ">

      <div class="col-4 localidade">
        <div class="media">
          <img class="align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_atentimento.png" alt="">
          <div class="media-body">
            <h2 class="mt-0">ATENDIMENTO</span></h2>
          </div>
        </div>
        <img class="bottom20 top10" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_atentimento.png" alt="">
        <div class="col-9 ml-auto media">
          <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
          <div class="media-body align-self-center">
            <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h4>
          </div>
        </div>
        <div class="col-9 ml-auto media">
          <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
          <div class="media-body align-self-bottom">
            <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h4>
          </div>
        </div>



        <div class="media top25">
          <img class="align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="">
          <div class="media-body">
            <h2 class="mt-0">COMO CHEGAR</span></h2>
          </div>
        </div>
        <img class="bottom20 top10" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_atentimento.png" alt="">
        <div class="col-11 ml-auto">
          <p class="text-left"><?php Util::imprime($config[endereco]); ?></p>
        </div>

      </div>

      <div class="col-8 bottom100 padding0">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   LOCALIDADE E TELEFONES -->
  <!--  ==============================================================  -->

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
