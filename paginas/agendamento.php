<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",9) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container titulo_marcar">
    <div class="row ">
      <div class="col-12 text-center top80 bottom50">
        <img class="right10 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_consulta.png" alt="">
        <?php $row2 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21);?>
        <h3><span><?php Util::imprime($row2[legenda_1]) ?></span></h3>

        <h3 class="top50">HORÁRIO DE FUNCIONAMENTO</h3>
        <p><?php Util::imprime($config[horario_1]); ?></p>

      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
    <div class="container">
      <div class="row ">
        <div class="col-4">
          <!-- ======================================================================= -->
          <!-- CARRINHO  -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/lista_itens_orcamento.php') ?>
          <!-- ======================================================================= -->
          <!-- FORMULARIO  -->
          <!-- ======================================================================= -->

        </div>

        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->
        <div class="col-8  fundo_formulario">

          <div class="form-row">
            <div class="col">
              <div class="form-group icon_form">
                <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="localidade" class="form-control fundo-form form-control-lg input-lg" placeholder="LOCALIDADE">
                <span class="fa fa-globe form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" id="data" name="data" class="form-control fundo-form form-control-lg  input-lg" placeholder="DATA" >
                <span class="fa fa-calendar form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" id="hora" name="hora" class="form-control fundo-form form-control-lg input-lg" placeholder="HORA">
                <span class="fa fa-clock-o form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col">
              <div class="form-group icon_form">
                <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="OBSERVAÇÕES"></textarea>
                <span class="fa fa-pencil form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="col-12 text-center padding0 ">
            <button type="submit" class="btn btn_formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>
        </div>

        <!--  ==============================================================  -->
        <!-- FORMULARIO CONTATOS-->
        <!--  ==============================================================  -->


      </div>

    </div>
  </form>


  <div class="container">
    <div class="row mapa top220">
      <div class="col-3">

        <div class="media">
          <img class="align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="">
          <div class="media-body">
            <h2 class="mt-0">COMO CHEGAR</span></h2>
          </div>
        </div>
        <img class="bottom20 top10" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_atentimento.png" alt="">
        <div class="col-11 ml-auto">
          <p class="text-left"><?php Util::imprime($config[endereco]); ?></p>
        </div>
      </div>
      <div class="col-9 bottom100 padding0">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){



  //  CADASTRO OS PRODUTOS SOLICITADOS
  // for($i=0; $i < count($_POST[qtd]); $i++){
  //   $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);
  //
  //   $produtos .= "
  //   <tr>
  //   <td><p>". $_POST[qtd][$i] ."</p></td>
  //   <td><p>". utf8_encode($dados[titulo]) ."</p></td>
  //   </tr>
  //   ";
  // }

  //  CADASTRO OS SERVICOS SOLICITADOS

  for($i=0; $i < count($_POST[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_tratamentos", "idtratamento", $_POST[idtratamento][$i]);
    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd_servico][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }




  //  ENVIANDO A MENSAGEM PARA O CLIENTE
   $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Nome: $_POST[nome] <br />
  Email: $_POST[email] <br />
  Telefone: $_POST[telefone] <br />
  Assunto: $_POST[localidade] <br />
  Data: $_POST[data] <br />
  Horário: $_POST[hora] <br />

  OBSERVAÇÕES: <br />
  ". nl2br($_POST[mensagem]) ." <br />

  <br />
  <h2> Itens selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>TRATAMENTOS</h4></td>
  </tr>
  $produtos
  </table>

  ";



  if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]),utf8_decode($_POST[email]))) {
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]),utf8_decode($_POST[email]));
    //unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}



?>
