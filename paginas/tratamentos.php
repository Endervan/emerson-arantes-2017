<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #ececec url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <?php $row1 = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 4);?>
      <div class="col-12 text-center top150 bottom95">
        <h3> <?php Util::imprime($row1[legenda_1]) ?></h3>
        <h3><span><?php Util::imprime($row1[legenda_2]) ?></span></h3>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->

  <!--  ==============================================================  -->
  <!--   TRATAMENTOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid">
    <div class="row tratamentos ">
      <div class="container bottom95">
        <div class="row ">
          <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>

          <div class="col-12 top40">
            <p><?php Util::imprime($row1[descricao]) ?></p>
          </div>



            <?php
            $result = $obj_site->select("tb_tratamentos");
            if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
                $i=0;
                ?>

                <div class="col-6 text-center top40">
                  <a href="<?php echo Util::caminho_projeto() ?>/tratamento/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">

                    <div class="card">
                      <img width="120" height="120" class=" top35 media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[imagem]); ?>" />
                      <div class="card-body">
                        <div>
                          <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
                        </div>
                        <div class="top15">
                          <p class="card-text desc_tratamento"><?php Util::imprime($row[descricao],200); ?></p>
                        </div>
                      </div>

                    </div>
                  </a>
                </div>

                <?php
                if ($i==1) {
                  echo "<div class='clearfix'>  </div>";
                }else {
                  $i++;
                }
              }
            }


          ?>



      </div>
    </div>


  </div>
</div>
<!--  ==============================================================  -->
<!--   TRATAMENTOS -->
<!--  ==============================================================  -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
