<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_tratamentos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/tratamentos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
  

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",5) ?>
<style>
.bg-interna{
  background:  #ececec url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'tratamentos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-12 titulo_dentro text-center top105 bottom120">
        <img width="120" height="120" class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>" alt="<?php Util::imprime($dados_dentro[imagem]); ?>" />
        <h3><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h3>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <div class="container bottom60">
    <div class="row tratamento_dentro ">

      <div class="col-8 ">
        <?php
        if ($dados_dentro[imagem_principal] == '') {
          echo "<div class='imagem_default'><img src='../imgs/default.png' alt=''></div>";
          ?>
          <?php
        }else{
          ?>
          <div class="">
            <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem_principal]",770, 327, array("class"=>"w-100", "alt"=>"$dados_dentro[titulo]")) ?>
          </div>
          <?php
        }
        ?>

        <div class="top15">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
        <div class="top20">
          <a href="javascript:void(0);" class="btn btn_tratamentos_orcamento" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
            <img class=" right10 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_consulta.png" alt="">  MARCAR UMA CONSULTA
          </a>
        </div>
      </div>
      <div class="col-4">

        <div class="col-12 bg_tratamento text-center">
          <a href="javascript:void(0);" class="btn btn_tratamentos_orcamento w-100" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
            <img class="right10 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_consulta.png" alt="">  MARCAR UMA CONSULTA
          </a>


          <!-- ======================================================================= -->
          <!-- telefones  -->
          <!-- ======================================================================= -->
          <div class="telefone_topo pb30 bottom30">
            <div class="row">
              <div class="col-6 padding0 media ">
                <img class="d-flex ml-1" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
                <div class="media-body align-self-center">
                  <h4 class="mt-0 ml-1"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h4>
                </div>
              </div>
              <div class="col-6 ml-auto padding0 media">
                <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
                <div class="media-body align-self-bottom">
                  <h4 class="mt-0 ml-1"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h4>
                </div>
              </div>
            </div>
          </div>
        </div>


          <?php /*
        <h5 class="mt-0">PERGUNTAS FREQUETES</h5>
        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/barra.png" alt="" />



        <!-- ======================================================================= -->
        <!-- PERGUNTAS FREQUETES  -->
        <!-- ======================================================================= -->
        <div id="accordion" role="tablist">
          <?php

          $result1 = $obj_site->select("tb_perguntas","LIMIT 3");
          if (mysql_num_rows($result1) > 0) {
            $b = 0;
            while($row1 = mysql_fetch_array($result1)){
              ?>

              <div class="card perguntas">
                <div class="card-header" role="tab" id="list-pergunta-<?php echo $b; ?>">
                  <h5 class="mb-0">
                    <a data-toggle="collapse" href="#accordion-list-pergunta-<?php echo $b; ?>" aria-expanded="true" aria-controls="accordion-list-pergunta-<?php echo $b; ?>">
                      <?php Util::imprime($row1[titulo]); ?>
                    </a>
                  </h5>
                </div>

                <div id="accordion-list-pergunta-<?php echo $b; ?>" class="collapse" role="tabpanel" aria-labelledby="list-pergunta-<?php echo $b; ?>" data-parent="#accordion">
                  <div class="card-body">
                    <p><?php Util::imprime($row1[descricao]); ?></p>
                  </div>
                </div>
              </div>

              <?php
              $b++;
            }
          }
          ?>

        </div>
        <!-- ======================================================================= -->
        <!-- PERGUNTAS FREQUETES  -->
        <!-- ======================================================================= -->
*/ ?>


        <h5 class=" top20 mt-0">O QUE ELAS DIZEM</h5>
        <img  src="<?php echo Util::caminho_projeto() ?>/imgs/barra.png" alt="" />
        <!-- ======================================================================= -->
        <!-- rodape    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/slider_comentarios.php') ?>
        <!-- ======================================================================= -->
        <!-- rodape    -->
        <!-- ======================================================================= -->

      </div>
    </div>
  </div>





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 270,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
