<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",6) ?>
<style>
.bg-interna{
  background: #eaeaea url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">

      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6);?>
      <div class="col-12 text-center top150 bottom120">
        <h3> <?php Util::imprime($row[legenda_1]) ?></h3>
        <h3><span><?php Util::imprime($row[legenda_2]) ?></span></h3>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container">
    <div class="row ">

      <div class="col-4 padding0">

        <!-- ======================================================================= -->
        <!-- telefones  -->
        <!-- ======================================================================= -->
        <div class="telefone_topo">
          <div class="row">
            <div class="col-6 ml-auto padding0 media">
              <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
              <div class="media-body align-self-bottom">
                <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?></h4>
              </div>
            </div>
            <div class="col-6 padding0 media">
              <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
              <div class="media-body align-self-center">
                <h4 class="mt-0 ml-2"><span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?></h4>
              </div>
            </div>
          </div>
        </div>
        <!-- ======================================================================= -->
        <!-- telefones  -->
        <!-- ======================================================================= -->



        <!-- ======================================================================= -->
        <!-- links  -->
        <!-- ======================================================================= -->
        <ul class="nav nav-pills text-center flex-column top20">
          <li class="nav-item  col-12">
            <a class="nav-link active" disabled href="#">
              <div class="media">
                <img class="d-flex align-self-center" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_fale.png" alt="">
                <div class="media-body">
                  <span class="clearfix mt-0">TIRE SUAS DÚVIDAS</span>FALE CONOSCO
                </div>
              </div>
            </a>
          </li>
          <?php /*
          <li class="nav-item  col-12 top10">
            <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
              <div class="media">
                <img class="d-flex align-self-center" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_trabalhe.png" alt="">
                <div class="media-body">
                  <span class="clearfix mt-0">FAÇA PARTE DA NOSSA EQUIPE</span>TRABALHE CONOSCO
                </div>
              </div>
            </a>
          </li>
          */ ?>

        </ul>
        <!-- ======================================================================= -->
        <!-- links  -->
        <!-- ======================================================================= -->

      </div>

      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->
      <div class="col-8  fundo_formulario">
        <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">

          <div class="form-row">
            <div class="col">
              <div class="form-group icon_form">
                <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="assunto" class="form-control fundo-form form-control-lg input-lg" placeholder="ASSUNTO">
                <span class="fa fa-star form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col">
              <div class="form-group icon_form">
                <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="col-12 text-center padding0 ">
            <button type="submit" class="btn btn_formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>

        </form>
      </div>

    </div>

    <!--  ==============================================================  -->
    <!-- FORMULARIO CONTATOS-->
    <!--  ==============================================================  -->

  </div>



  <div class="container">
    <div class="row mapa top220">
      <div class="col-3">

        <div class="media">
          <img class="align-self-center mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_localizacao.png" alt="">
          <div class="media-body">
            <h2 class="mt-0">COMO CHEGAR</span></h2>
          </div>
        </div>
        <img class="bottom20 top10" src="<?php echo Util::caminho_projeto() ?>/imgs/barra_atentimento.png" alt="">
        <div class="col-11 ml-auto">
          <p class="text-left"><?php Util::imprime($config[endereco]); ?></p>
        </div>
      </div>
      <div class="col-9 bottom100 padding0">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
    $texto_mensagem = "
  Assunto: ".($_POST[assunto])." <br />
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  assunto: ".($_POST[assunto])." <br />
  Celular: ".($_POST[celular])." <br />

  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";

    if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
        Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
        Util::alert_bootstrap("Obrigado por entrar em contato.");
        unset($_POST);
    }else{
        Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
    }

}

?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
