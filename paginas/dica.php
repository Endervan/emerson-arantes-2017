<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/dicas");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background:  #ececec url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 153px center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2);?>
      <div class="col-12 text-center top150 bottom120">
        <h3> <?php Util::imprime($row[legenda_1]) ?></h3>
        <h3><span><?php Util::imprime($row[legenda_2]) ?></span></h3>
      </div>

    
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <div class="container bottom60">
    <div class="row">

      <div class="col-12 dicas_dentro">
        <div class="top30">
          <h5><?php Util::imprime($dados_dentro[titulo]); ?></h5>
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_dica.jpg" alt="">
        </div>
        <div class="top20">
          <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",1170, 408, array("class"=>"w-100", "alt"=>"$dados_dentro[titulo]")) ?>
        </div>
        <div class="">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>

      </div>
    </div>
  </div>





  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 270,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
