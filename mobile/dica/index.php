
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>

	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>





</head>

<body class="bg-interna">


	<?php
	$voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php") ?>


	<!-- ======================================================================= -->
	<!--  titulo geral -->
	<!-- ======================================================================= -->
	<div class="row">
		<div class="col-12 text-center localizacao-pagina1 titulo_emp">
			<?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2);?>
			<div class="">
				<h3><?php Util::imprime($row[legenda_1]) ?></h3>
				<h3><span><?php Util::imprime($row[legenda_2]) ?></span></h3>
			</div>

		</div>
	</div>
	<!-- ======================================================================= -->
	<!--  titulo -->
	<!-- ======================================================================= -->



	<!-- ======================================================================= -->
	<!--  descricao -->
	<!-- ======================================================================= -->
	<div class="row bottom50">
		<div class="col-12">
			<h2 class="top30"><?php echo Util::imprime($dados_dentro[titulo]) ?></h2>
			<div class="col-8 padding0 bottom10 top10">
				<amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_dica.jpg"
					width="270"
					height="5"
					layout="responsive"
					alt="AMP">
				</amp-img>
			</div>

			<div class="clearfix">	</div>

			<amp-img
			on="tap:lightbox1"
			role="button"
			tabindex="0"

			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
			layout="responsive"
			width="450"
			height="250"
			alt="<?php echo Util::imprime($dados_dentro[titulo]) ?>">

		</amp-img>
		<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>

		<div class="dicas_dentro">
			<p ><?php echo Util::imprime($dados_dentro[descricao]) ?></p>
		</div>

	</div>

</div>
<!-- ======================================================================= -->
<!--  descricao -->
<!-- ======================================================================= -->





<?php require_once("../includes/rodape.php") ?>

</body>



</html>
