
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>
</head>



<body class="bg-index">
  <div class="row ">
    <div class="col-12 text-center topo">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="150" width="480"></amp-img>
    </div>
  </div>


  <div class="row font-index">

    <div class="col-12 text-center topo">
      <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
        <p><?php Util::imprime($row1[descricao]) ?></p>
    </div>
    <div class="clearfix">  </div>



    <!-- ======================================================================= -->
    <!-- MENU   -->
    <!-- ======================================================================= -->
    <div class="col-4 top5">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_clinica.png"
          width="152"
          height="155"
          layout="responsive"
          alt="empresa">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top5">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/tratamentos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_tratamento.png"
          width="152"
          height="152"
          layout="responsive"
          alt="tratamentos">
        </amp-img>
      </a>
    </div>


    <div class="col-4 top5">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contatos_home.png"
          width="152"
          height="152"
          layout="responsive"
          alt="contatos">
        </amp-img>
      </a>
    </div>

    <div class="clearfix">  </div>




    <div class="col-4 col-offset-2 top30">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/convenios">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_convenio.png"
          width="152"
          height="152"
          layout="responsive"
          alt="convenios">
        </amp-img>
      </a>
    </div>

<?php /*
    <div class="col-4 top30">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhos-sociais">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_trabalhe.png"
          width="152"
          height="152"
          layout="responsive"
          alt="trabalhos-sociais">
        </amp-img>
      </a>
    </div>

*/ ?>

    <div class="col-4 top30">
      <a href="<?php Util::imprime($config[link_place]); ?>">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde.png"
          width="152"
          height="152"
          layout="responsive"
          alt="dicas">
        </amp-img>
      </a>
    </div>
    <!-- ======================================================================= -->
    <!-- MENU   -->
    <!-- ======================================================================= -->





  </div>


  <?php require_once("./includes/rodape.php") ?>


</body>



</html>
