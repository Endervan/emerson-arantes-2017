
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_tratamentos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/tratamentos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>

	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>





</head>

<body class="bg-interna">


	<?php
	$voltar_para = 'tratamentos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php") ?>


	<!-- ======================================================================= -->
	<!--  titulo geral -->
	<!-- ======================================================================= -->
	<div class="row">
		<div class="col-12 localizacao-pagina text-center titulo_emp">
			<amp-img
			class="col-4 col-offset-4 media-object clearfix"
			layout="responsive"
			height="100"
			width="100"
			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem]); ?>" alt="<?php Util::imprime($dados_dentro[titulo]); ?>" >
		</amp-img>
		<h3 class="clearfix"><span><?php Util::imprime($dados_dentro[titulo]); ?></span></h3>


		<a href="<?php echo Util::caminho_projeto() ?>/mobile/agendamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=servico" title="SOLICITAR UM ORÇAMENTO">
			<amp-img
			class="col-6 col-offset-3 top25"
			layout="responsive"
			height="22"
			width="100"
			src="<?php echo Util::caminho_projeto() ?>/imgs/marca_consilta.png" alt="" >
		</amp-img>
	</a>


</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->
<!-- ======================================================================= -->
<!--  CONTATOS -->
<!-- ======================================================================= -->
<div class="row telefone_topo top10">

	<div class="col-6">

		<div class="media">
			<amp-img class="align-self-center mr-1" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="A EMPRESA"
				height="20"
				width="20">
			</amp-img>

			<div class="media-body">
				<h5 class="mt-0">
					<amp-fit-text
					width="150"
					height="40"
					layout="responsive">
					<span>	<?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?>
				</amp-fit-text>
			</h5>
		</div>
	</div>
	</a>
</div>

	<div class="col-6">

		<div class="media">
			<amp-img class="align-self-center mr-1" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="A EMPRESA"
				height="20"
				width="20">
			</amp-img>

			<div class="media-body">
				<h5 class="mt-0">
					<amp-fit-text
					width="150"
					height="40"
					layout="responsive">
					<span>	<?php Util::imprime($config[ddd2]); ?></span> <?php Util::imprime($config[telefone2]); ?>
				</amp-fit-text>
			</h5>
		</div>
	</div>
	</a>
</div>

</div>

<!-- ======================================================================= -->
<!-- telefones  -->
<!-- ======================================================================= -->




<div class="row top40">
	<div class="col-12 text-center">

		<?php
		$result = $obj_site->select("tb_galerias_servicos", "AND id_servico = '$dados_dentro[0]' ");
		if(mysql_num_rows($result) == 0){ ?>

			<amp-carousel id="carousel-with-preview"
			width="480"
			height="400"
			layout="responsive"
			type="slides"
			autoplay
			delay="4000"
			>

			<amp-img
			on="tap:lightbox2"
			role="button"
			tabindex="0"
			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($dados_dentro[imagem_principal]); ?>"
			layout="responsive"
			width="480"
			height="334"
			alt="default">

		</amp-img>
	</amp-carousel>
	<amp-image-lightbox id="lightbox2"layout="nodisplay">	</amp-image-lightbox>

<?php }else{?>
	<amp-carousel id="carousel-with-preview"
	width="480"
	height="334"
	layout="responsive"
	type="slides"
	autoplay
	delay="4000"
	>

	<?php
	$i = 0;
	$result = $obj_site->select("tb_galerias_servicos", "and id_servico = $dados_dentro[idtratamento]");
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)){
			?>

			<amp-img
			on="tap:lightbox1"
			role="button"
			tabindex="0"

			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
			layout="responsive"
			width="480"
			height="400"
			alt="<?php echo Util::imprime($row[titulo]) ?>">

		</amp-img>
		<?php
	}
	$i++;
}


?>

</amp-carousel>

<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>


<?php } ?>

</div>
</div>





<!-- ======================================================================= -->
<!--  PRODUTOS DETALHES -->
<!-- ======================================================================= -->
<div class="row detalhes_tratamentos">

	<div class="col-12 top10">
		<p ><?php echo Util::imprime($dados_dentro[descricao]) ?></p>
	</div>

</div>
<!-- ======================================================================= -->
<!--  PRODUTOS DETALHES -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- slider    -->
<!-- ======================================================================= -->
<?php require_once('../includes/caroucel_comentarios.php') ?>
<!-- ======================================================================= -->
<!-- slider    -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
