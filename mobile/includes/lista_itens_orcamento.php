<div class="row">
  <div class="col-12 lista-produto-titulo">

    <?php if(count($_SESSION[solicitacoes_servicos])+count($_SESSION[solicitacoes_produtos]) == 0): ?>

      <tr class="col-12">
        <div class="text-center top10 bottom10">
          <h1>NENHUMA CONSULTA SELECIONADO</h1>
          <a
          on="tap:my-lightbox22"
          role="button"
          tabindex="0"
          class="btn btn-primary" >
          ESCOLHA UMA CONSULTA
        </a>
      </div>
    </tr>


  <?php else: ?>
    <h5 class="">TRATAMENTOS SELECIONADO(S)</h5>


    <table class="col-12 padding0 tabela-orcamento ">
      <tbody>


        <?php
        if(count($_SESSION[solicitacoes_produtos]) > 0){
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <tr>
              <td>
                <amp-img
                height="50" width="50"
                src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]) ?>">
              </amp-img>
            </td>
            <td class="text-uppercase"><?php Util::imprime($row[titulo]); ?></td>
            <td class="text-center">
              <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
              <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
            </td>
            <td class="text-center">
              <a href="?action=del&id=<?php echo $i; ?>&tipo_orcamento=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                <i class="fa fa-times-circle fa-2x"></i>
              </a>
            </td>
          </tr>
          <?php
        }
      }
      ?>

      <?php
      if(count($_SESSION[solicitacoes_servicos]) > 0){
        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
          $row = $obj_site->select_unico("tb_tratamentos", "idtratamento", $_SESSION[solicitacoes_servicos][$i]);
          ?>
          <tr>
            <td>
              <amp-img
              height="50" width="50"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
            </amp-img>
          </td>
          <td class="text-uppercase"><?php Util::imprime($row[titulo]); ?></td>
          <td class="text-center">
            <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
            <input name="idtratamento[]" type="hidden" value="<?php echo $row[0]; ?>"  />
          </td>
          <td class="text-center">
            <a href="?action=del&id=<?php echo $i; ?>&tipo_orcamento=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
              <i class="fa fa-times-circle fa-2x"></i>
            </a>
          </td>
        </tr>
        <?php
      }
    }
    ?>


  <?php endif; ?>



</tbody>
</table>


<amp-lightbox id="my-lightbox22"
layout="nodisplay">
<div class="lightbox"
on="tap:my-lightbox22.close"
role="button"
tabindex="0">



<div class="col-12 categoria text-uppercase">

  <div class="list-group">
    <a href="#" class="list-group-item list-group-item-action active ">
      Selecione Uma Consulta
    </a>
    <?php

    $result = $obj_site->select("tb_tratamentos");
    if (mysql_num_rows($result) > 0) {
      $i = 0;
      while($row1 = mysql_fetch_array($result)){
        ?>
        <a class="list-group-item list-group-item-action" href="<?php echo Util::caminho_projeto() ?>/mobile/agendamento/index.php?action=add&id=<?php echo $row1[0] ?>&tipo_orcamento=servico" title="SOLICITAR UM ORÇAMENTO">
          <?php Util::imprime($row1[titulo]); ?>
        </a>

        <?php
        if ($i == 1) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }

      }
    }
    ?>


  </div>



</div>



</amp-lightbox>

</div>

</div>
