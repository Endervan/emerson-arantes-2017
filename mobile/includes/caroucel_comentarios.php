<div class="row top40">
  <div class="col-12">
    <h6><span>O QUE ELAS DIZEM</span></h6>

    <amp-img
    class="bottom10 top10"
    src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/dizem.png"
    width="130"
    height="5"
    alt="<?php echo Util::imprime($row[titulo]) ?>">

  </amp-img>
  </div>
  <div class="col-12 slider_comentario">




    <amp-carousel  id="carousel-with-preview"
    width="480"
    height="600"
    layout="responsive"
    type="slides"
    autoplay
    delay="30000"
    >

    <?php
    $i = 0;
    $result = $obj_site->select("tb_comentarios");
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
        ?>


        <div class="top20 slider">
          <amp-img
          class="rounded-circle col-6 col-offset-3 bottom10"
          on="tap:lightbox5"
          role="button"
          tabindex="0"

          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
          layout="responsive"
          width="200"
          height="200"
          alt="<?php echo Util::imprime($row[titulo]) ?>">

        </amp-img>
        <div class="col-10 col-offset-1 text-center top10 caption clearfix">
          <?php /*<h2 class="text-uppercase "><span><?php Util::imprime($row[titulo]); ?></span></h2>*/ ?>
          <p class="text-uppercase "><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>

      <?php
    }
    $i++;
  }

  ?>

</amp-carousel>
<amp-image-lightbox id="lightbox5" layout="nodisplay">	</amp-image-lightbox>

<!-- ======================================================================= -->
<!--  PRODUTOS DETALHES -->
<!-- ======================================================================= -->
</div>
</div>
