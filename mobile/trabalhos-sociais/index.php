<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="row">
    <div class="col-12 text-center localizacao-pagina1 titulo_emp">
      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8);?>
        <h3> <?php Util::imprime($row[legenda_1]) ?></h3>
        <h3><span><?php Util::imprime($row[legenda_2]) ?></span></h3>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--   TRABALHOS SOCIAIS -->
  <!--  ==============================================================  -->

  <div class="row  emp_descricao  bottom50">
    <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>

    <div class="col-12  bottom25 top20">
      <P><span><?php Util::imprime($row1[descricao]) ?></span></P>
    </div>



    <?php
    $result = $obj_site->select("tb_trabalhos");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;
        ?>

        <div class="col-12 top25">
          <div class="card">
            <div>
              <h2 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
            </div>

              <amp-img
              class="media-object"
              layout="responsive"
              height="268"
              width="450"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
            </amp-img>
            <div class="card-body">

              <div>
                <p class="card-text"><?php Util::imprime($row[descricao],200); ?></p>
              </div>
            </div>

          </div>
      </div>

      <?php
      if ($i==1) {
        echo "<div class='clearfix'>  </div>";
      }else {
        $i++;
      }
    }
  }


  ?>



</div>
<!--  ==============================================================  -->
<!--   TRABALHOS SOCIAIS -->
<!--  ==============================================================  -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
