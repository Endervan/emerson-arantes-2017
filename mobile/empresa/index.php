<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>








  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="row">
    <div class="col-12 text-center localizacao-pagina titulo_emp">

      <?php $row = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1);?>
      <div class="top50">
        <h3> <span><?php Util::imprime($row[legenda_1]) ?></span></h3>
        <h6><?php Util::imprime($row[legenda_2]) ?></h6>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   DESCRICOES -->
  <!--  ==============================================================  -->
  <div class="row emp_descricao bottom50">
<?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
    <div class="col-12 top20">
      <p class="card-text top10"><?php Util::imprime($row1[descricao]) ?></p>

      <?php $row2 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
      <p class="card-text top10"><?php Util::imprime($row2[descricao]) ?></p>
    </div>


<?php /*
    <?php $row2 = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
    <?php $row3 = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
    <?php $row4 = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
    <?php $row5 = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>

    <div class="col-12 text-center top40 card">
      <amp-img
      class="col-2 col-offset-5"
      layout="responsive"
      height="70"
      width="45"
      src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa01.png" alt="" >
    </amp-img>

    <div class="card-body clearfix">
      <h1 class="top10"><?php Util::imprime($row3[titulo]) ?></h1>
      <p class="card-text top10"><?php Util::imprime($row3[descricao]) ?></p>
    </div>
  </div>

  <div class="col-12 text-center top40 card">
    <amp-img
    class="col-2 col-offset-5"
    layout="responsive"
    height="70"
    width="45"
    src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa02.png" alt="" >
  </amp-img>
  <div class="card-body clearfix">
    <h1 class="top10"><?php Util::imprime($row4[titulo]) ?></h1>
    <p class="card-text top10"><?php Util::imprime($row4[descricao]) ?></p>
  </div>
</div>

<div class="col-12 text-center top40 card">
  <amp-img
  class="col-2 col-offset-5"
  layout="responsive"
  height="70"
  width="45"
  src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa03.png" alt="" >
</amp-img>
<div class="card-body clearfix">
  <h1 class="top10"><?php Util::imprime($row5[titulo]) ?></h1>
  <p class="card-text top10"><?php Util::imprime($row5[descricao]) ?></p>
</div>
</div>

*/ ?>


</div>
<!--  ==============================================================  -->
<!--   DESCRICOES -->
<!--  ==============================================================  -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
